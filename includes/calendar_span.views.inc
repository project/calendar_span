<?php

/**
 * Implementation of hook_views_plugins
 */
function calendar_span_views_plugins() {
  $path = drupal_get_path('module', 'calendar_span');
  $calendar_path = drupal_get_path('module', 'calendar');

  $data = array(
    'module' => 'calendar_span', // This just tells our themes are elsewhere.
    'style' => array(
      'parent' => array(
        // this isn't really a display but is necessary so the file can
        // be included.
        'no ui' => TRUE,
        'handler' => 'calendar_view_plugin_style',
        'path' => "$calendar_path/includes",
        'parent' => '',
      ),
      'calendar_span_style' => array(
        'title' => t('Calendar Span Date'),
        'help' => t('Displays Views results in a calendar with spanned datetimes.'),
        'handler' => 'calendar_span_view_plugin_style',
        'path' => "$path/includes",
        'parent' => 'calendar_style',
        'theme' => 'calendar_span_week',
        'theme path' => "$path/theme",
        'additional themes' => array(
          'calendar_span_day' => 'style',
          ),
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses grouping' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'even empty' => TRUE,
      ),
    ),
  );
  return $data;
}
