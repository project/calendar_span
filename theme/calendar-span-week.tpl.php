<?php
/**
 * @file
 * Template to display a view as a calendar week.
 * 
 * @see template_preprocess_calendar_week.
 *
 * $day_names: An array of the day of week names for the table header.
 * $rows: The rendered data for this week.
 * 
 * For each day of the week, you have:
 * $rows['date'] - the date for this day, formatted as YYYY-MM-DD.
 * $rows['datebox'] - the formatted datebox for this day.
 * $rows['empty'] - empty text for this day, if no items were found.
 * $rows['all_day'] - an array of formatted all day items.
 * $rows['items'] - an array of timed items for the day.
 * $rows['items'][$time_period]['hour'] - the formatted hour for a time period.
 * $rows['items'][$time_period]['ampm'] - the formatted ampm value, if any for a time period.
 * $rows['items'][$time_period]['values'] - An array of formatted items for a time period.
 * 
 * $view: The view.
 * $min_date_formatted: The minimum date for this calendar in the format YYYY-MM-DD HH:MM:SS.
 * $max_date_formatted: The maximum date for this calendar in the format YYYY-MM-DD HH:MM:SS.
 * 
 */
//dsm('Display: '. $display_type .': '. $min_date_formatted .' to '. $max_date_formatted);
//dsm($rows);
/*
dsm($items);
dsm($pretty_times);
dsm($columns);
dsm($rows);
 */
?>

<style type="text/css">

.calendar-span-cell-empty {
  background-color: blue;
}
.calendar-span-cell-item {
  background-color: white;
}

.calendar-calendar td.calendar-free-block:hover {
    cursor:pointer;
  background-color: blue;
}
.calendar-calendar td.calendar-free-block.o:hover {
  cursor: default;
  background-color: #FFFFFF;
}

.calendar-calendar div.calendar-free-block {
  background: transparent;
}

</style>

<div class="calendar-calendar"><div class="week-view">
<table>
  <thead>
    <tr>
      <th class="calendar-agenda-hour"><?php print $by_hour_count > 0 ? t('Time') : ''; ?></th>
      <?php foreach ($day_names as $cell): ?>
        <th class="<?php print $cell['class']; ?>">
          <?php print $cell['data']; ?>
        </th>
      <?php endforeach; ?>
    </tr>
  </thead>



  <tbody>
    <tr>
      <td class="<?php print $agenda_hour_class ?>">
    <?php foreach ($pretty_times as $time): ?>
        <div class="calendar-agenda-hour" style="height: 72px;">
        <span class="calendar-hour"><?php print $time['hour']; ?></span>
        <span class="calendar-ampm"><?php print $time['ampm']; ?></span>
      </div>
      <?php endforeach; ?>  
      </td>

    <?php foreach ($items as $day => $values): ?>
      <td class="calendar-agenda-items">
        <?php foreach ($values['values'] as $time => $item): ?>
           <div class="calendar-span-cell<?php print isset($values['span'][$time]) ? "-item" : "-empty"; ?> " style="height: <?php print isset($values['span'][$time]) ? $values['span'][$time][0]*72 : '72'; ?>px;">

              
              <?php print !empty($item) ? $item[0]." span:".$values['span'][$time][0] : ''; ?> 
</div>
        <?php endforeach; ?>
      </td>
    <?php endforeach; ?>
</tr>


  </tbody>
</table>
</div></div>
