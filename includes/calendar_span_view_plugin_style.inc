<?php
class calendar_span_view_plugin_style extends calendar_view_plugin_style {
  function init(&$view, &$display) {
    parent::init($view, $display);
    
    // TODO make this an option setting.
    $view->date_info->style_show_empty_times = TRUE ;
    $view->date_info->grouping = $this->options['grouping'];
    
  }
  /**
   * Set default options
   */
  function options(&$options) {
    parent::options($options);
    $options['orienation'] = 'verticle';
    $options['unique'] = TRUE;
    $options['grouping'] = array('default' => '');
    return $options;
  }

  /**
   * Style options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form,$form_state);
    $form['orienation'] = array(
      '#title' => t('Orientation'),
      '#type' => 'select',
      '#options' => array('horizontal' => t('Horizontal'), 'verticle' => t('Verticle')),
      '#default_value' => $this->options['orientation'] ,
      '#description' => t('Horizontal or verticle orientation. '),
      );
    $form['unique'] = array(
      '#title' => t('Filter out overlapping events'),
      '#type' => 'checkbox',
      '#default_value' =>  $this->options['unique'] ,
      '#description' => t('Filter out events whose time overlaps.'),
      );
    // Only fields-based views can handle grouping.  Style plugins can also exclude
    // themselves from being groupable by setting their "use grouping" definiton
    // key to FALSE.
    // @TODO: Document "uses grouping" in docs.php when docs.php is written.
    if ($this->uses_fields() && $this->definition['uses grouping']) {
      $options = array('' => t('<None>'));
      $options += $this->display->handler->get_field_labels();

      // If there are no fields, we can't group on them.
      if (count($options) > 1) {
        $form['grouping'] = array(
          '#type' => 'select',
          '#title' => t('Grouping field'),
          '#options' => $options,
          '#default_value' => $this->options['grouping'],
          '#description' => t('You may optionally specify a field by which to group the records. Leave blank to not group.'),
        );
      }
    }
    unset($form['groupby_field']); 

  }

  /**
   * Render the calendar attachment style.
   */
  function render() {

    // Group the rows according to the grouping field, if specified.
    $sets = $this->render_grouping($this->view->result, $this->options['grouping']);

    // Adjust the theme to match the currently selected default.
    // Only the month view needs the special 'mini' class,
    // which is used to retrieve a different, more compact, theme.
    $this->view->date_info->mini = FALSE;
    $this->definition['theme'] = 'calendar_span_'. $this->view->date_info->granularity;
    $this->view->date_info->hide_admin_links = TRUE;

    return theme($this->theme_functions(), $this->view, $this->options, $sets, array());
  }
  /**
   * Calendar argument date fields used in this view.
   */
  function date_fields() {
    $date_fields = array();
    $calendar_fields = date_api_fields($this->view->base_table);
    $arguments = $this->display->handler->get_option('arguments');
    foreach ($arguments as $name => $argument) {
      if (isset($argument['date_fields'])) {
        foreach ($argument['date_fields'] as $date_field) {
          $field = $calendar_fields['name'][$date_field];
          $handler = views_get_handler($field['table_name'], $field['field_name'], 'field');
          if ($handler) {
            $date_fields[$date_field] = $field;
            $date_fields[$date_field]['name'] = $handler->ui_name();
          }
        }
      }
    }
    return ($date_fields);
  }

}
