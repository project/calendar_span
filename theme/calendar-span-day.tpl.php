<?php
/**
 * @file
 * Template to display a view as a calendar day, grouped by time
 * and optionally organized into columns by a field value.
 * 
 * @see template_preprocess_calendar_day.
 *
 * $rows: The rendered data for this day.
 * $rows['date'] - the date for this day, formatted as YYYY-MM-DD.
 * $rows['datebox'] - the formatted datebox for this day.
 * $rows['empty'] - empty text for this day, if no items were found.
 * $rows['all_day'] - an array of formatted all day items.
 * $rows['items'] - an array of timed items for the day.
 * $rows['items'][$time_period]['hour'] - the formatted hour for a time period.
 * $rows['items'][$time_period]['ampm'] - the formatted ampm value, if any for a time period.
 * $rows['items'][$time_period][$column]['values'] - An array of formatted 
 *   items for a time period and field column.
 * 
 * $view: The view.
 * $columns: an array of column names.
 * $min_date_formatted: The minimum date for this calendar in the format YYYY-MM-DD HH:MM:SS.
 * $max_date_formatted: The maximum date for this calendar in the format YYYY-MM-DD HH:MM:SS.
 * 
 * The width of the columns is dynamically set using <col></col> 
 * based on the number of columns presented. The values passed in will
 * work to set the 'hour' column to 10% and split the remaining columns 
 * evenly over the remaining 90% of the table.
 */
//dsm('Display: '. $display_type .': '. $min_date_formatted .' to '. $max_date_formatted);
//dsm($rows);
//dsm($columns);
?>
<style type="text/css">

.tableBorder {
    background-color: #36648B;
}

/*
Individal time cells for the scheduler
*/
td.scheduleTimes, .scheduleTimes td {
    font-size: 11px;
    color: #333333;
    text-align: left;
    background-color: #EDEDED;
    padding-left: 2px;
}
/*
Date cell for the scheduler
*/
td.scheduleDate, .scheduleDate td, .scheduleDateHeader, td.scheduleDateCurrent {
    font-size: 11px;
    color: #FFFFFF;
    background-color: #A2B5CD;
    text-align: center;
    padding: 2px;
}

td.scheduleDateCurrent {
  font-weight: bold;
  background-color: #5E7FB1;
}

.scheduleDateHeader {
  border-bottom: 1px #36648B solid;
}

td.scheduleDate, td.scheduleDateHeader a {
  color: #FFFFFF;
}

/*
Resource name cell for the scheduler
*/
td.resourceName, td.resourceNameOver {
    color: #333333;
    font-size: 11px;
    background-color: #EDEDED;
    padding-left: 5px;
    padding-top: 2px;
    padding-bottom: 2px;
    padding-right: 5px;
}

td.resourceNameOver {
    background-color: #FAFAFA;
}

.scheduleTable {
    border-collapse: separate;
}

.r0 td, .ro0 td
{
    padding: 2px;
    background-color: #FFFFFF;
    font-size: 11px;
}

.r1 td, .ro1 td
{
    padding: 2px;
    background-color: #F8FAEA;
    font-size: 11px;
}

.r0 td:hover, .r1 td:hover
{
    cursor:pointer;
    background-color: green;
}
.r0 td.o:hover
{
  cursor: default;
  background-color: #FFFFFF;
}

.r1 td.o:hover
{
  cursor: default;
  background-color: #F8FAEA;
}

.r0:hover td.resourceName, .r1:hover td.resourceName
{
    background-color: #f7f18b;
}

.r0 td.resourceName:hover, .r1 td.resourceName:hover
{
  cursor: default;
  background-color: #EDEDED;
}
#calendar_div, .calendar {
  width: auto;
}

</style>
<table class="scheduleTable" cellspacing="0" cellpadding="1" border="0" width="100%">
  <tbody>
    <tr class="tableBorder">
      <td>
        <table class="scheduleTable" cellspacing="1" cellpadding="0" border="0" width="100%">
          <tbody>
            <tr class="scheduleTimes">

<td width="15%" class="scheduleDate" rowspan="2">&nbsp;</td>
  <?php foreach ($times as $hour): ?>
<td colspan="4"><?php print $hour['hour']; ?><?php print $hour['ampm']; ?>
</td>
<?php endforeach; ?>

            </tr>

<tr class="scheduleTimes">
  <?php foreach ($times as $hour): ?>
    <?php $count = 4; while($count): ?>
<td colspan="1">&nbsp;</td>
    <?php $count--; endwhile; ?>
<?php endforeach; ?>
</tr>


<?php foreach($rows as $group => $items): ?>
<tr class="r0"><td class="resourceName"><a href="javascript: reserve('r','sc14c89465caa983','1283659200','','sc14c89454257f7e','0','0');">
<?php print $group; ?></a>
</td>

<?php foreach($items['items'] as $time): ?>
<td colspan="<?php print isset($time['span']) ? $time['span'] : '1'; ?>" >
<?php print isset($time['values']['items']) ? $time['values']['items'][0] : '&nbsp;'; ?>
</td>
<?php endforeach; ?>
<td colspan="1" >
&nbsp;
</td>
</tr>
<?php endforeach; ?>


</tbody></table>

